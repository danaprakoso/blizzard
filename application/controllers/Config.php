<?php

class Config extends CI_Controller {
	
	public function refresh_external_configs() {
		$data = file_get_contents("http://www.vpngate.net/api/iphone/");
		$this->db->query("DELETE FROM `external_configs`");
		$this->db->query("ALTER TABLE `external_configs` AUTO_INCREMENT=1");
		$configs = explode("\n", $data);
		for ($i=2; $i<sizeof($configs)-1; $i++) {
			$config = $configs[$i];
			$configNodes = explode(",", $config);
			try {
				$ip = array_key_exists(1, $configNodes)?$configNodes[1]:NULL;
				$country = array_key_exists(5, $configNodes)?$configNodes[5]:NULL;
				$countryCode = array_key_exists(6, $configNodes)?$configNodes[6]:NULL;
				$configString = array_key_exists(14, $configNodes)?$configNodes[14]:NULL;
				if ($ip != NULL && $country != NULL && $countryCode != NULL && $configString != NULL) {
					$this->db->insert('external_configs', array(
						'ip' => $ip,
						'country_code' => strtolower($countryCode),
						'country' => $country,
						'user' => 'vpn',
						'pass' => 'vpn',
						'config' => $configString
					));
				}
			} catch (Exception $e) {
			}
		}
	}
	
	public function get() {
		echo json_encode($this->db->get('configs')->result_array());
	}
	
	public function get_external() {
		echo json_encode($this->db->get('external_configs')->result_array());
	}
}
