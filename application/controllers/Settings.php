<?php

class Settings extends CI_Controller {
	
	public function get_current_version() {
		echo $this->db->get('settings')->row_array()['current_version'];
	}
	
	public function get() {
		echo json_encode($this->db->get('configs')->result_array());
	}
	
	public function get_external() {
		echo json_encode($this->db->get('external_configs')->result_array());
	}
}
