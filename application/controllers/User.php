<?php

class User extends CI_Controller {
	
	public function get_speed_test_url() {
		$setting = $this->db->get("settings")->row_array();
		$downloadURL = $setting['speed_test_download_url'];
		$uploadURL = $setting['speed_test_upload_url'];
		echo json_encode(array(
			'download_url' => $downloadURL,
			'upload_url' => $uploadURL
		));
	}
}
